{
"Rooms": {
		"r": ["Room C01", "Room C02", "Room C03", "Room C04", "Room C05"]
	},
"Classes": [
{
"Subject": "Data Structure And Algorithm",
"Type": "P",
"Professor": "Manoj Sharma",
"Group": [
"301",
"302",
"303",
"304",
"305",
"306"
],
"Allowed_Class": "r",
"Duration": "1"
},
{
"Subject": "Operation Research",
"Type": "P",
"Professor": "Manoj Sharma",
"Group": [
"301",
"302",
"303",
"304",
"305",
"306"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Human Computer Interaction",
"Type": "P",
"Professor": "Hari Shakya",
"Group": [
"301",
"302",
"305",
"306"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Operation Research",
"Type": "V",
"Professor": "Kishor Karki",
"Group": [
"301",
"303",
"304",
"305"
],
"Allowed_Class": "r",
"Duration": "3"
},
{
"Subject": "System Analysis And Design",
"Type": "P",
"Professor": "Milan Rajbhandari",
"Group": [
"301",
"303",
"304"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "System Analysis And Design",
"Type": "V",
"Professor": "Amrita Dutta",
"Group": [
"301",
"303",
"304"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Data Structure And Algorithm",
"Type": "V",
"Professor": "John Doe",
"Group": [
"301",
"303",
"306"
],
"Allowed_Class": "r",
"Duration": "3"
},
{
"Subject": "Embedded System",
"Type": "P",
"Professor": "Babu Bhaiya",
"Group": [
"301",
"302",
"304"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Embedded System",
"Type": "V",
"Professor": "Atma Gandhi",
"Group": [
"301"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Human Computer Interaction",
"Type": "V",
"Professor": "Nirjan Koirala",
"Group": [
"301",
"302",
"306"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Computer Graphics",
"Type": "V",
"Professor": "Katerine Scott",
"Group": [
"302",
"303"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Data Structure And Algorithm",
"Type": "V",
"Professor": "John Doe",
"Group": [
"302",
"303",
"304"
],
"Allowed_Class": "r",
"Duration": "1"
},
{
"Subject": "Computer Graphics",
"Type": "P",
"Professor": "Sagar Niraula",
"Group": [
"302",
"303",
"305",
"306",
"301"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Programming Fundamentals",
"Type": "P",
"Professor": "Babu Bhaiya",
"Group": [
"302",
"304",
"303"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Programming Fundamentals",
"Type": "V",
"Professor": "John Smith",
"Group": [
"302",
"304",
"305"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Operation Research",
"Type": "V",
"Professor": "Kishor Karki",
"Group": [
"302",
"304",
"306"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Data Structure And Algorithm",
"Type": "V",
"Professor": "John Doe",
"Group": [
"306",
"307",
"301",
"302"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Operation Research",
"Type": "V",
"Professor": "Kishor Karki",
"Group": [
"303",
"301",
"302"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "System Analysis And Design",
"Type": "V",
"Professor": "Amrita Dutta",
"Group": [
"303",
"301",
"306",
"302"
],
"Allowed_Class": "r",
"Duration": "2"
},
{
"Subject": "Computer Graphics",
"Type": "V",
"Professor": "Katerine Scott",
"Group": [
"309",
"309a",
"305",
"306"
],
"Allowed_Class": "r",
"Duration": "2"
}
]
}